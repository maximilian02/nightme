package com.nightme;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Max on 24/06/2015.
 */
public class EventActivity extends Activity {
    private static String TAG = EventActivity.class.getName();
    private static long SLEEP_TIME = 5;    // Sleep for some time

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_flyer);

        Button infoButton = (Button) findViewById(R.id.infoButton);

        infoButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "+ Info button pressed:", Toast.LENGTH_LONG).show();
            }
        });
    }
}
