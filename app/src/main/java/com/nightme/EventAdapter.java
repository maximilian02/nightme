package com.nightme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nightme.entities.Event;

import java.util.List;

/**
 * Created by Max on 16/05/2015.
 */
public class EventAdapter extends BaseAdapter {
    private Context context;
    private List<Event> items;

    public EventAdapter(Context context, List<Event> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item, parent, false);
        }

        // Set data into the view.
        ImageView eventImage = (ImageView) rowView.findViewById(R.id.eventImage);
        TextView eventTitle = (TextView) rowView.findViewById(R.id.eventTitle);
        TextView eventDate = (TextView) rowView.findViewById(R.id.eventDate);

        Event item = this.items.get(position);
        eventTitle.setText(item.getTitle());
        eventDate.setText(item.getDate());
        eventImage.setImageResource(item.getEventImage());

        return rowView;
    }
}
