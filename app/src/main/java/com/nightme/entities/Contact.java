package com.nightme.entities;

/**
 * Created by Max on 16/05/2015.
 */
public class Contact {
    private String phone;
    private String mail;
    private String name;

    public Contact(){}

    public Contact(String phone, String mail, String name) {
        this.phone = phone;
        this.mail = mail;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
