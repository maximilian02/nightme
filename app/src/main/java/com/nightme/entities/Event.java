package com.nightme.entities;

/**
 * Created by Max on 16/05/2015.
 */

public class Event {
    private String title;
    private String date;
    private Contact contact;
    private boolean confirmed;
    private int price;
    private String buyTicketsUrl;
    private String promotionalFlyerUrl;
    private int eventImage;
    private String eventId;
    //private ArrayList<Comment> comments;
    //private ArrayList<Artist> artists;
    //private Place place;

    public Event(){}

    public Event(int eventImage, String title, String date, String eventId){
        this.eventId = eventId;
        this.eventImage = eventImage;
        this.title = title;
        this.date = date;
    }

    public Event(String title, String date, Contact contact, boolean confirmed, int price, String buyTicketsUrl, String promotionalFlyerUrl, String eventId){
        this.eventId = eventId;
        this.title = title;
        this.date = date;
        this.contact = contact;
        this.confirmed = confirmed;
        this.price = price;
        this.buyTicketsUrl = buyTicketsUrl;
        this.promotionalFlyerUrl = promotionalFlyerUrl;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBuyTicketsUrl() {
        return buyTicketsUrl;
    }

    public void setBuyTicketsUrl(String buyTicketsUrl) {
        this.buyTicketsUrl = buyTicketsUrl;
    }

    public String getPromotionalFlyerUrl() {
        return promotionalFlyerUrl;
    }

    public void setPromotionalFlyerUrl(String promotionalFlyerUrl) {
        this.promotionalFlyerUrl = promotionalFlyerUrl;
    }

    public int getEventImage() {
        return eventImage;
    }

    public void setEventImage(int eventImage) {
        this.eventImage = eventImage;
    }
}
